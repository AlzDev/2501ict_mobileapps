//
//  DetailViewController.swift
//  Friends
//
//  Created by Alan on 18/05/2016.
//  Copyright © 2016 Alan. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

	@IBOutlet weak var detailDescriptionLabel: UILabel!
	@IBOutlet weak var firstName: UILabel!
	@IBOutlet weak var lastName: UILabel!


	var detailItem: AnyObject? {
		didSet {
		    // Update the view.
		    self.configureView()
		}
	}

	func configureView() {
		// Update the user interface for the detail item.
		if let detail = self.detailItem {
		    if let label = self.detailDescriptionLabel {
		        label.text = detail.valueForKey("firstName")!.description
		    }
			if let label = self.firstName {
				label.text = detail.valueForKey("lastName")!.description
			}
			if let label = self.lastName {
				label.text = detail.valueForKey("address")!.description
			}
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		self.configureView()
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}


}

