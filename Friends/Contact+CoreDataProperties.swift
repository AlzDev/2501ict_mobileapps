//
//  Contact+CoreDataProperties.swift
//  Friends
//
//  Created by Alan on 18/05/2016.
//  Copyright © 2016 Alan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Contact {

    @NSManaged var address: String?
    @NSManaged var firstName: String?
    @NSManaged var image: NSData?
    @NSManaged var imageURL: String?
    @NSManaged var lastName: String?
    @NSManaged var sites: NSSet?

}
